const express = require("express");
const router = express.Router();


const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

// CREATE COURSE


router.post("/",auth.verify,(req,res)=>{

	const data={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseControllers.addCourse(data).then(resultFromController=>res.send(resultFromController));


});
	

// RETRIEVE COURSES

router.get("/all",(req,res)=>{

	courseControllers.getAllCourses().then(resultFromController=>res.send(resultFromController));
});

// ACTIVE COURSES

router.get("/",(req,res)=>{
	courseControllers.getAllACtive().then(resultFromController=>res.send(resultFromController));
});


// GET SPECIFIC COURSE

router.get("/:courseId",(req,res)=>{
	courseControllers.getCourse(req.params).then(resultFromController=>res.send(resultFromController));
});


// UPDATING A COURSE

router.put("/:courseId",auth.verify,(req,res)=>{


	courseControllers.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));


});

// ACTIVITY: ARCHIVE A COURSE

router.put("/archive/:courseId",auth.verify,(req,res)=>{


	courseControllers.archiveCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));


});


// STRETCH GOAL: ACTIVATING A COURSE


router.put("/activate/:courseId",auth.verify,(req,res)=>{


	courseControllers.activateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));


});










module.exports = router;


