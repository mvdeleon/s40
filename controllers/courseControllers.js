const Course = require("../models/Course");

const auth = require("../auth");



// module.exports.addCourse = (reqBody,data)=>{

	
	
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price

// 	});



// 	if(data==true){
// 	return newCourse.save().then((course,error)=>{
// 		if(error){
// 				return false;
// 			}else{
// 				return true;

// 			};
// 				});

// 	}else{

// 			return false
// 		};


// 	};





module.exports.addCourse = (data)=>{

	if(data.isAdmin){

		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		return false;
	}

};


// Retrieve ALL courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result=>{
		return result;
	});
};

// Retrieving all active courses

module.exports.getAllACtive = () => {
	return Course.find({isActive:true}).then(result=>{
		return result;
	})
}

// Retrieving specific course

module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	});
};

// Update a course

module.exports.updateCourse = (reqParams,reqBody) =>{

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	
	});
};

// ACTIVITY: ARCHIVING A COURSE

module.exports.archiveCourse = (reqParams,reqBody) =>{

	let archivedCourse = {
		isActive: false

	}
	return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	
	});
};

// STRETCH GOAL: ACTIVATING A COURSE

module.exports.activateCourse = (reqParams,reqBody) =>{

	let activatedCourse = {
		isActive: true

	}
	return Course.findByIdAndUpdate(reqParams.courseId,activatedCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	
	});
};
