const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");



module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};

// REGISTRATIONS

module.exports.registerUser = (reqBody)=>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNumber : reqBody.mobileNumber,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

// USER AUTHENTICATION

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};


module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result=>{result.password = "";
		return result;
});
}

// ACTIVITY


// module.exports.getProfile = (reqBody) =>{
// 	return User.findOne({id:reqBody.id}).then(result=>{
// 		if(result==null){
// 			return false;
// 		}
// 		result.password = " ";
// 		return result
		
		
// 	});
// };


